function LazyValue(v){
    this.value = v;
}

function lazy(v){
    return new LazyValue(v);
}

function pull(L){
    if(L instanceof LazyValue){
        return pull(L.value());
    }
    return L;
}


module.exports = {
    lazy : lazy,
    pull : pull
};
