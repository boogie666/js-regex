const {
    lazy,
    pull
} = require("./lazy");
const _ = require("lodash");



function lazy_getter(f) {
    var result = undefined;
    return function() {
        if (result === undefined) {
            result = f.call(this);
        }
        return result;
    };
}

function isa(type) {
    return function(x) {
        return x instanceof type;
    };
}

function create(type) {
    return function(...args) {
        return new type(...args);
    };
}

function Empty() {}
Empty.prototype.derive = function() {
    return this;
};
Empty.prototype.nullable = function() {
    return false;
};

const empty = new Empty();
const is_empty = isa(Empty);

function Eps() {}
Eps.prototype.derive = function() {
    return empty;
};
Eps.prototype.nullable = function() {
    return true;
};


const eps = new Eps();
const is_eps = isa(Eps);

function Value(x) {
    this.value = x;
}
Value.prototype.derive = function(x) {
    return this.value === x ? eps : empty;
};
Value.prototype.nullable = function() {
    return false;
};

const value = create(Value);
const is_value = isa(Value);

function Pred(func) {
    this.function = func;
}
Pred.prototype.derive = function(x) {
    return this.function(x) ? eps : empty;
};
Pred.prototype.nullable = function() {
    return false;
};
const pred = create(Pred);
const is_pred = isa(Pred);

function lift(x) {
    if (x instanceof Function) {
        return x;
    }
    return _ => x;
}

function Alt(left, right) {
    this.this = lazy_getter(left);
    this.that = lazy_getter(right);
}

Alt.prototype.nullable = function() {
    return this.this().nullable() || this.that().nullable();
};

Alt.prototype.derive = function(x) {
    return new Alt(
        () => this.this().derive(x),
        () => this.that().derive(x)
    );
};

const _alt = function(a,b){
    return new Alt(lift(a), lift(b));
};
const is_alt = isa(Alt);

function alt(...args) {
    return args.reduce(_alt);
}

function Cat(left, right) {
    this.left = lazy_getter(left);
    this.right = lazy_getter(right);
}
Cat.prototype.nullable = function() {
    return this.left().nullable() && this.right().nullable();
};



Cat.prototype.derive = function(x) {
    if (this.left().nullable()) {
        return new Alt(
            () => this.right().derive(x),
            () => new Cat(
                () => this.left().derive(x),
                () => this.right()
            )
        );
    } else {
        return new Cat(
            () => this.left().derive(x),
            () => this.right()
        );
    }
};
const _cat = function(a,b){
    return new Cat(lift(a), lift(b));
};
const is_cat = isa(Cat);

function cat(...args) {
    return args.reduce(_cat);
}

function Rep(lang) {
    this.lang = lazy_getter(lang);
}
Rep.prototype.nullable = function() {
    return true;
};


Rep.prototype.derive = function(x) {
    return new Cat(
        () => this.lang().derive(x),
        () => this
    );
};

const is_rep = isa(Rep);

function rep(lang) {
    return new Rep(lift(lang));
}

function plus(L) {
    return cat(L, rep(L));
}

function maybe(L) {
    return alt(eps, L);
}

function derive(L, c) {
    return L.derive(c);
}


function fixedPoint(bottom, body) {
    let cache = new Map();
    let changed = undefined;
    let running = false;
    let visited = undefined;
    return function f(x) {
        let is_cached = cache.has(x);
        let cached = is_cached ? cache.get(x) : bottom;
        let is_run = running;

        if (is_cached && !is_run) {
            return cached;
        } else if (is_run && visited.has(x)) {
            return is_cached ? cached : bottom;
        } else if (is_run) {
            visited.set(x, true);
            let new_value = body(x);
            if (new_value !== cached) {
                changed = true;
                cache.set(x, new_value);
            }
            return new_value;
        } else if (!is_cached && !is_run) {
            changed = true;
            running = true;
            let v = bottom;
            while (changed) {
                changed = false;
                visited = new Map();
                v = f(x);
            }
            return v;
        }

        throw "Bad Case";
    };
}

const nullable = fixedPoint(false, function(L) {
    if (is_empty(L)) {
        return false;
    }
    if (is_eps(L)) {
        return true;
    }
    if (is_value(L)) {
        return false;
    }
    if (is_pred(L)) {
        return false;
    }
    if (is_rep(L)) {
        return true;
    }
    if (is_alt(L)) {
        return nullable(L.this()) || nullable(L.that());
    }
    if (is_cat(L)) {
        return nullable(L.left()) && nullable(L.right());
    }
    throw new Error(L + " is not nullable");
});

function match(L, str) {
    return nullable(str.split("").reduce(derive, L));
}



const whitespace = rep(pred(function(x){
    return x.trim() === "";
}));

const digit = pred(function(x){
    return "0123456789".indexOf(x) !== -1;
});

const character = pred(function(x){
    x = x.toLowerCase();
    return "abcdefghijklmnopqrstuvwxyz".indexOf(x) !== -1;
});

const word = cat(
    character,
    rep(alt(character, digit))
);

const string = cat(whitespace, value("\""), maybe(word), value("\""));
const number = plus(digit);
const _true = cat(value("t"), value("r"), value("u"), value("e"));
const _false = cat(value("f"), value("a"), value("l"), value("s"), value("e"));
const boolean = alt(_true, _false);

const array_start = cat(whitespace, value("["), whitespace);
const array_end = cat(whitespace, value("]"), whitespace);
const array_separator = cat(whitespace, value(","), whitespace);
const array = cat(
    array_start,
    rep(cat(
        ()=>json,
        array_separator,
    )),
    maybe(()=>json),
    array_end
);

const object_start = cat(whitespace, value("{"), whitespace);
const object_end = cat(whitespace, value("}"), whitespace);
const object_key_separator = cat(whitespace, value(":"), whitespace);

const object = cat(
    object_start,
    rep(cat(
        string,
        object_key_separator,
        ()=>json,
    )),
    object_end
);


const json = alt(
    string, number, boolean,
    array, object
);


console.log(match(json, "[1,{\"hello\" : []}, 2, 3, [1,2,3, true, \"hello\"]]"));
